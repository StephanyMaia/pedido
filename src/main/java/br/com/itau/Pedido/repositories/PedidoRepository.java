package br.com.itau.Pedido.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.Pedido.models.Pedido;

public interface PedidoRepository  extends CrudRepository<Pedido, Integer>{

}
